import { api } from 'boot/axios'

export const login = data => {
  return api.post('/admin-login', data)
}

export const logout = () => {
  return api.post('/logout')
}
