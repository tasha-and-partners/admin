import { api } from 'boot/axios'

const endpoint = '/workshops'

export const getWorkshops = params => {
  return api.get(`${endpoint}`, { params })
}

export const getWorkshop = id => {
  return api.get(`${endpoint}/${id}`)
}

export const createWorkshop = data => {
  return api.post(`${endpoint}`, data)
}

export const updateWorkshop = (id, data) => {
  return api.put(`${endpoint}/${id}`, data)
}

export const deleteWorkshop = id => {
  return api.delete(`${endpoint}/${id}`)
}
