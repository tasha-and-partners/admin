// Routes
import authRoutes from './auth'
import dashboardRoutes from './dashboard'

const routes = [
  ...authRoutes,
  ...dashboardRoutes,

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    name: 'page404',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
