const dashboardRoutes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', name: 'Dashboard', component: () => import('pages/Index.vue') },
      {
        path: 'admins',
        component: () => import('layouts/DefaultLayout.vue'),
        children: [
          { path: '', name: 'Admins', component: () => import('pages/Users/Admins/Index.vue') },
          { path: 'create', name: 'CreateAdmin', component: () => import('pages/Users/Admins/Create.vue') },
          { path: ':id/edit', name: 'EditAdmin', component: () => import('pages/Users/Admins/Edit.vue') }
        ]
      },
      {
        path: 'speakers',
        component: () => import('layouts/DefaultLayout.vue'),
        children: [
          { path: '', name: 'Speakers', component: () => import('pages/Users/Speakers/Index.vue') },
          { path: 'create', name: 'CreateSpeaker', component: () => import('pages/Users/Speakers/Create.vue') },
          { path: ':id/edit', name: 'EditSpeaker', component: () => import('pages/Users/Speakers/Edit.vue') }
        ]
      },
      {
        path: 'moderators',
        component: () => import('layouts/DefaultLayout.vue'),
        children: [
          { path: '', name: 'Moderators', component: () => import('pages/Users/Moderators/Index.vue') },
          { path: 'create', name: 'CreateModerator', component: () => import('pages/Users/Moderators/Create.vue') },
          { path: ':id/edit', name: 'EditModerator', component: () => import('pages/Users/Moderators/Edit.vue') }
        ]
      },
      {
        path: 'topics',
        component: () => import('layouts/DefaultLayout.vue'),
        children: [
          { path: '', name: 'Topics', component: () => import('pages/Topics/Index.vue') },
          { path: 'create', name: 'CreateTopic', component: () => import('pages/Topics/Create.vue') },
          { path: ':id/edit', name: 'EditTopic', component: () => import('pages/Topics/Edit.vue') }
        ]
      },
      {
        path: 'workshops',
        component: () => import('layouts/DefaultLayout.vue'),
        children: [
          { path: '', name: 'Workshops', component: () => import('pages/Workshops/Index.vue') },
          { path: 'create', name: 'CreateWorkshop', component: () => import('pages/Workshops/Create.vue') },
          { path: ':id/edit', name: 'EditWorkshop', component: () => import('pages/Workshops/Edit.vue') }
        ]
      }
    ]
  }
]

export default dashboardRoutes
