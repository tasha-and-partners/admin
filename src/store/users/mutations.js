export const setUsers = (state, users) => {
  state.users = [...users]
}

export const setUser = (state, user) => {
  state.user = { ...user }
}
