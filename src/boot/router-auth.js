// Services
import nextRouteFactory from '../router/services/next-route-factory.service'

// Navigation guards before each request
const beforeEach = async (to, from, next) => {
  const nextRoute = await nextRouteFactory.build(to)

  nextRoute ? next(nextRoute) : next()
}

export default ({ router }) => {
  router.beforeEach((to, from, next) => beforeEach(to, from, next))
}
